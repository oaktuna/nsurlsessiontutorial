//
//  ViewController.m
//  NSURLSessionTutorial
//
//  Created by Admin on 20/07/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController () {
    UIView *loadingView;
    NSString *titleString;
    NSMutableArray *arr_products;
    NSMutableDictionary *dict_products;
}

@end

@implementation ListViewController

static NSString const *baseApiUrl = @"http://52.28.140.212";
static NSString *const kGetHTTPMethod = @"GET";
static NSString *const kPostHTTPMethod = @"POST";


#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getDataFromService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Util Methods
- (void)initiateLoadingView:(UIView *)baseView {
    loadingView = [[UIView alloc]initWithFrame:CGRectMake((([UIScreen mainScreen].bounds.size.width - 100.0f)/2.0f), (([UIScreen mainScreen].bounds.size.height - 100.0f)/2.0f), 100.0f, 100.0f)];
    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
    loadingView.layer.cornerRadius = 5;
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingView addSubview:activityView];
    
    UILabel *lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(-5.0f, 58.0f, 110, 30)];
    lblLoading.text = @"Bekleyiniz...";
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.font = [UIFont fontWithName:@"Roboto-Bold" size:15.0];
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [loadingView addSubview:lblLoading];
    
    [baseView addSubview:loadingView];
}

- (void)removeLoadingView {
    [loadingView setHidden:YES];
    [loadingView removeFromSuperview];
}

- (void)getDataFromService {
    /*
     
     ---------------------
     DOWNLOAD SOME DATA
     ---------------------
     
    // 1
    NSString *dataUrl = [NSString stringWithFormat:@"%@%@",baseApiUrl,@"/v2/venues/search"];
    NSURL *url = [NSURL URLWithString:dataUrl];
    
    // 2
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              
                                              
                                              if (error != nil) {
                                                  //TODO: Handle Error Here !
                                              }
                                              else {
                                                  
                                              }
                                              
                                          }];
    
    // 3
    [downloadTask resume];
     */
    
    /*
     
    --------------------
    //DOWNLOAD AN IMAGE
    --------------------
     
    //1
    NSURL *url = [NSURL URLWithString:
                  @"http://upload.wikimedia.org/wikipedia/commons/7/7f/Williams_River-27527.jpg"];
    
    // 2
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       // 3
                                                       UIImage *downloadedImage = [UIImage imageWithData:
                                                                                   [NSData dataWithContentsOfURL:location]];
                                                   }];
    
    // 4	
    [downloadPhotoTask resume];
    */
    
    
    
    //--------------------
    //POSTING DATA
    //--------------------
    
//    [self.tblListView setHidden:YES];
    [self initiateLoadingView:self.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gettingProductsCompleted:) name:@"successGettingAllProducts" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gettingProductsFailed:) name:@"failGettingAllProducts" object:nil];
    
    // 1
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseApiUrl,@"/api/v1/product/list"]];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = kPostHTTPMethod;
    
    NSDictionary *queryParams = @{@"type" : @"Yıkama"};
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:queryParams
                                                   options:kNilOptions error:&error];
    
    if (!error) {
        // 4
        NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                    fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                        
                    if (error != nil) {
                        NSLog(@"Error Here !");
                    }
                    else {
                        NSError *jsonErr = nil;
                        id payload = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonErr];
                        
                        if(jsonErr == nil && [payload isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = (NSDictionary *)payload;
//                            NSLog(@"%@",dict);
                            
                            NSNumber *result = [dict objectForKey:@"success"];
                            
                            if ([result boolValue] == YES) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"successGettingAllProducts" object:self userInfo:dict];
                            }
                            else {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"failGettingAllProducts" object:self userInfo:dict];
                            }
                        }
                    }
                    
                }];
        
        // 5
        [uploadTask resume];
    }
    else {
         NSLog(@"Error Here !");
    }
}

- (void)createReloadAnimation:(UITableView *)tableView {
    [UIView transitionWithView:tableView duration:0.5f options:UIViewAnimationOptionTransitionCurlDown animations:^(void) {
        [tableView reloadData];
    } completion:NULL];
}

#pragma mark - Getting product request notification
- (void)gettingProductsCompleted:(NSNotification *)notification {
    
    // dispatch_get_main_queue TO PREVENT "This application is modifying the autolayout engine from a background thread etc." warning
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"successGettingAllProducts" object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"failGettingAllProducts" object:nil];
        
        dict_products = [[notification userInfo] mutableCopy];
        arr_products = [[dict_products result] mutableCopy];
        
        if ([arr_products count] == 0) {
            NSLog(@"Error Here !");
        }
        else {
            /*for(int i = 0; i < [arr_products count]; i++) {
                NSLog(@"%@ --- %@",[[arr_products objectAtIndex:i] productName],[[arr_products objectAtIndex:i] productId]);
            }*/
            [self createReloadAnimation:self.tblListView];
        }
        
        [self removeLoadingView];
        
        /*[UIView animateWithDuration:0.5 animations:^{
            self.tblListView.alpha = 0.0;
            self.tblListView.alpha = 1.0;
        } completion:^(BOOL success) {
            if (success) {
                [self.tblListView setHidden:NO];
            }
        }];*/
    });
    
}

- (void)gettingProductsFailed:(NSNotification *)notification {
    
    // dispatch_get_main_queue TO PREVENT "This application is modifying the autolayout engine from a background thread etc." warning
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"successGettingAllProducts" object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"failGettingAllProducts" object:nil];
        
        [self removeLoadingView];
        
        /*[UIView animateWithDuration:0.5 animations:^{
            self.tblListView.alpha = 0.0;
            self.tblListView.alpha = 1.0;
        } completion:^(BOOL success) {
            if (success) {
                [self.tblListView setHidden:NO];
            }
        }];*/
        
    });
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arr_products count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
     return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 60.f;
}
 
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
     UIView *headerView = [UIView new];
     [headerView setBackgroundColor:[UIColor blackColor]];
     
     UILabel *lblTitle = [[UILabel alloc] init];
     [lblTitle setFrame:CGRectMake(10.0f, 2.0f, [UIScreen mainScreen].bounds.size.width - 50.0f, 60.0f)];
     [lblTitle setText:@"This is only one HEADERVIEW in only one SECTION"];
     [lblTitle setNumberOfLines:2];
     [lblTitle setTextColor:[UIColor redColor]];
     [lblTitle setTextAlignment:NSTextAlignmentCenter];
     [headerView addSubview:lblTitle];
     
     return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [UIView new];
    [footerView setBackgroundColor:[UIColor purpleColor]];
    
    UILabel *lblTitle = [[UILabel alloc] init];
    [lblTitle setFrame:CGRectMake(10.0f, 2.0f, [UIScreen mainScreen].bounds.size.width - 50.0f, 60.0f)];
    [lblTitle setText:@"This is only one FOOTERVIEW in only one SECTION"];
    [lblTitle setNumberOfLines:2];
    [lblTitle setTextColor:[UIColor cyanColor]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [footerView addSubview:lblTitle];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 71.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCell" forIndexPath:indexPath];
    
    [self configureCustomCell:(ListViewCell*)cell atIndexPath:indexPath tableRef:tableView];
    
    return cell;
}

- (void)configureCustomCell:(ListViewCell*)cell atIndexPath:(NSIndexPath *)indexPath tableRef:(UITableView *)tableView {
    if (indexPath.row % 2 == 0) {
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    else {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    
    cell.lbl1.text = [NSString stringWithFormat:@"%@",[[arr_products objectAtIndex:indexPath.row] productName]];
    cell.lbl2.text = [NSString stringWithFormat:@"id:%@",[[arr_products objectAtIndex:indexPath.row] productId]];
}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    MyCollectionViewController *collectionVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"myCollectionView"];
//    [self.navigationController pushViewController:collectionVC animated:YES];
}*/

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toCollectionViewSegue"]) {
        // Get reference to the destination view controller
        MyCollectionViewController *myCollectionVCref = [segue destinationViewController];
        // Get indexPath from sender
        NSIndexPath *indexPath = [self.tblListView indexPathForCell:sender];
        
        myCollectionVCref.strTitle = [[arr_products objectAtIndex:indexPath.row] productName];
        myCollectionVCref.arrItems = arr_products;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return YES;
}

@end
