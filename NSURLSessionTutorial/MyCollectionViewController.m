//
//  MyCollectionViewController.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 24/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "MyCollectionViewController.h"

@interface MyCollectionViewController ()

@end

@implementation MyCollectionViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTintColor:[UIColor greenColor]];
    [self.navigationItem setTitle:self.strTitle];
    [ImageDownloadHandler handlerSharedInstance].imageDownloadDelegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CollectionView Methods
//Kaç bölümden oluşacagını belirtir.
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//Bolumde kac eleman olacagını belirtir.
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.arrItems count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCollectionViewCell" forIndexPath:indexPath];
    cell.myLabel.text = [[self.arrItems objectAtIndex:indexPath.row] productName];
    
    if ([[[self.arrItems objectAtIndex:indexPath.row] productImage] isEqualToString:@""] || [[self.arrItems objectAtIndex:indexPath.row] productImage] == nil) {
        //NSLog(@"There is no image for %ld index item",(long)indexPath.row);
    }
    else {
//        if (cell.myImageView.image == nil) {
            [[ImageDownloadHandler handlerSharedInstance] imageFor:[[self.arrItems objectAtIndex:indexPath.row] productImage] cellRef:cell indexRef:indexPath.row];
//        }
    }

//    cell.myImageView.image = [UIImage imageNamed:imageStrings];
    
    return cell;
}

//Her bir hucrenin genişlik ve yuksekligini belirtir.
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(150.0, 178.0);
}

//Hucre arası ve ekran uzerindeki boslukları belirtir.
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"selected collection view item %ld",(long)indexPath.row);
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CollectionDetailViewController *collectionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"collectionDetailView"];
    [self.navigationController pushViewController:collectionDetailVC animated:YES];
}

#pragma mark - ImageDownloadHandler delegate
- (void)imageDownloadedSuccessfully:(UIImage *)cellImage cellRef:(MyCollectionViewCell *)collectionCell indexRef:(NSInteger)cellIndex {
    if (cellImage != nil) {
        //NSLog(@"let set image %ld",(long)cellIndex);
        collectionCell.myImageView.image = cellImage;
    }
}

@end
