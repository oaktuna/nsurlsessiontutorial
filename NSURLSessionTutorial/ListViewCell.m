//
//  ListCell.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 23/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "ListViewCell.h"

@implementation ListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
