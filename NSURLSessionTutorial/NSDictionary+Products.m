//
//  NSDictionary+Products.m
//  NSURLSessionTutorial
//
//  Created by Admin on 21/07/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "NSDictionary+Products.h"

@implementation NSDictionary (Products)

- (NSArray *)result {
    return self[@"result"];
}

- (NSString *)productName {
    return self[@"name"];
}

- (NSNumber *)productId {
    return self[@"product_id"];
}

- (NSString *)productImage {
    return self[@"thumb"];
}

@end
