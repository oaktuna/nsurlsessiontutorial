//
//  CarouselTableViewCell.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 25/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "CarouselTableViewCell.h"

#define SLIDER_HEIGHT 230 //Cell 260 boyunda, cell'in icindeki slider 230 boyunda

@implementation CarouselTableViewCell

- (id)initWithArray:(NSString *)reuseIdentifier imageArrayRef:(NSArray *)imagesArray {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.autoresizesSubviews = YES;
        //[self.contentView setFrame:CGRectMake(0, 0,CGRectGetWidth([[UIScreen mainScreen] bounds]),100)];
        //[self setFrame:self.contentView.frame];
        [self.contentView setBackgroundColor:[UIColor cyanColor]];
        
        //slider view
        self.sliderController = [[BaseSliderController alloc]initWithDataSource:imagesArray size:CGSizeMake([UIScreen mainScreen].bounds.size.width, SLIDER_HEIGHT) imageSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, SLIDER_HEIGHT)];
        
        [self.sliderController.view setBackgroundColor:[UIColor clearColor]];
        //[self.sliderController.view setFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, SLIDER_HEIGHT)];
        
        [self.sliderController setDelegate:self];
        
        //adding to content view
        [self.contentView addSubview:self.sliderController.view];

    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Slider Methods
- (void)sliderImageClicked:(UITapGestureRecognizer *)tapGestureRecognizer indexRef:(NSInteger)imageIndex {
    NSLog(@"%d",imageIndex);
}

- (void)sliderWillBeginDragging:(BaseSliderController *)slider {
    
}

- (void)sliderDidEndDecelerating:(BaseSliderController *)slider {
    
}

@end
