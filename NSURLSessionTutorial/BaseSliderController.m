//
//  BaseSliderController.m
//  BUYER
//
//  Created by Omer Aktuna on 3/24/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "BaseSliderController.h"

@implementation BaseSliderController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithDataSource:(NSArray*)dataSource size:(CGSize)containerSize imageSize:(CGSize)imageSize {
    self = [super init];
    
    if (self) {
        self.dataSource = dataSource;
        self.containerSize = containerSize;
        self.imageSize = imageSize;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //image container
    UIView *imageContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _imageSize.width * self.dataSource.count, _containerSize.height)];
    
    [imageContainer setBackgroundColor:[UIColor greenColor]];
    
    for (int i = 0; i < self.dataSource.count; i++) {
        
//        NSString *urlString = self.dataSource[i];
//          NSString *urlString = @"https://www.zizigo.com/site_media/uploads/banner/zizigo_mobil_bugun-50tl-uzeri-kargo-bedava-2.jpg";
        
//        if (urlString) {
            CGRect imageFrame = CGRectMake(_imageSize.width * i, 0, _imageSize.width, _imageSize.height);
            
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:imageFrame];
            
            //TODO: SET IMAGE
            [imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",[self.dataSource objectAtIndex:i]]]]; //@"slider_%d.jpg",i
            imageView.tag = i;
//            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
            [imageView addGestureRecognizer:tap];
        
            [imageContainer addSubview:imageView];

            /*NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        
            AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
            [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                //TODO: SET IMAGE
                [imageView setImage:responseObject];
                imageView.tag = i;
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
                [imageView addGestureRecognizer:tap];
                
                [imageContainer addSubview:imageView];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Image error: %@", error.description);
            }];
            
            [requestOperation start];*/
//        }
    }
    
    //scroll view
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _containerSize.width, _containerSize.height)];
    
    [_scrollView setContentSize:imageContainer.bounds.size];
    [_scrollView addSubview:imageContainer];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_scrollView setDelegate:self];
    
    self.pageControl = [[StyledPageControl alloc]initWithFrame:CGRectMake((([UIScreen mainScreen].bounds.size.width - 105.0) / 2.0), 230.0, 105.0, 25.0)];
    [self.pageControl setStrokeWidth:0];
    [self.pageControl setGapWidth:4];
    [self.pageControl setDiameter:10];
    
    [self.pageControl setCurrentPage:0];
    [self.pageControl setPageControlStyle:PageControlStyleDefault];
    [self.pageControl setCoreNormalColor:[UIColor colorWithRed:255.0 / 255.0
                                        green:255.0 / 255.0 blue:255.0 / 255.0 alpha:1]];
    [self.pageControl setCoreSelectedColor:[UIColor colorWithRed:189.0 / 255.0
                                        green:195.0 / 255.0 blue:199.0 / 255.0 alpha:1]];
    self.pageControl.alpha = 1.0f;
    
    [self.pageControl setNumberOfPages:[self.dataSource count]];
    
    if ([self.pageControl numberOfPages] < 2) {
        self.pageControl.hidden = YES;
    }
    
    [self.view addSubview:_scrollView];
   	[self.view addSubview:self.pageControl];
    [self.view bringSubviewToFront:_pageControl];
}

#pragma mark - Util
-(void)imageTapped:(UITapGestureRecognizer *)gesture {
    UIView *view = gesture.view;
    
    if([self.delegate respondsToSelector:@selector(sliderImageClicked:indexRef:)]) {
        [self.delegate sliderImageClicked:gesture indexRef:view.tag];
    }
}


#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = _scrollView.bounds.size.width;
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    [self.pageControl setCurrentPage:page];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if([self.delegate respondsToSelector:@selector(sliderWillBeginDragging:)]) {
        [self.delegate sliderWillBeginDragging:self];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {    
    if([self.delegate respondsToSelector:@selector(sliderDidEndDecelerating:)]) {
        [self.delegate sliderDidEndDecelerating:self];
    }
}


@end
