//
//  ListCell.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 23/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;


@end
