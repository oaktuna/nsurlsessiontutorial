//
//  CollectionDetailViewController.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 25/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "CollectionDetailViewController.h"


#define isIOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define TEST_STRING @"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum."

@interface CollectionDetailViewController ()

@end

@implementation CollectionDetailViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellTextSize = [self textSizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0f] constrainWidth:
                    [UIScreen mainScreen].bounds.size.width - 5.0f lineBreakMode:NSLineBreakByWordWrapping text:TEST_STRING];
    
    self.tblDetailView.delegate = self;
    self.tblDetailView.dataSource = self;
    
    [self.tblDetailView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 260.0f;
    }
    else if (indexPath.row == 1) {
        return self.cellTextSize.size.height;
    }
    else {
        return 71.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"carouselCell"];
        CarouselTableViewCell *carouselCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(carouselCell == nil) {
            carouselCell = [[CarouselTableViewCell alloc] initWithArray:CellIdentifier imageArrayRef:@[@"slider_0",@"slider_1",@"slider_2"]];
            [carouselCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        return carouselCell;
    }
    else if (indexPath.row == 1) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"textCell"];
        TextViewCell *textCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(textCell == nil) {
            textCell = [[TextViewCell alloc] initWithText:TEST_STRING reuseRef:CellIdentifier textSizeRef:self.cellTextSize];
            [textCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        return textCell;

    }
    else {
        UITableViewCell *standardCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        standardCell.detailTextLabel.text = [NSString stringWithFormat:@"inde of cell %ld", (long)indexPath.row];
        
        [standardCell setBackgroundColor:[UIColor redColor]];
        
        return standardCell;
    }
}

/*- (void)configureCustomCell:(ListViewCell*)cell atIndexPath:(NSIndexPath *)indexPath tableRef:(UITableView *)tableView {
    if (indexPath.row % 2 == 0) {
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    else {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
}*/

#pragma mark - Util
- (CGRect)textSizeWithFont:(UIFont *)font constrainWidth:(CGFloat)width lineBreakMode:(NSLineBreakMode)lineBreakMode text:(NSString *)text {
    
    CGRect sizeOfTextForGivenWidthConstrain;
    
    if (isIOS7) {
        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                              font, NSFontAttributeName,
                                              nil];
        
        CGRect rect = [text boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine)
                                      attributes:attributesDictionary
                                         context:nil];
        CGSize size = rect.size;
        size.height = ceilf(size.height);
        size.width = ceilf(size.width);
        sizeOfTextForGivenWidthConstrain = rect;
    }
    else {
        sizeOfTextForGivenWidthConstrain = [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:font} context:nil];
    }
    
    return sizeOfTextForGivenWidthConstrain;
}


@end
