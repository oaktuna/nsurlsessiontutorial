//
//  TextViewCell.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 26/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewCell : UITableViewCell

- (id)initWithText:(NSString *)cellText reuseRef:(NSString *)reuseIdentifier textSizeRef:(CGRect)cellTextSize;

@property(nonatomic,retain) UILabel *lblText;

@end
