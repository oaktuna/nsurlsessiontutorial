//
//  ImageDownloadHandler.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 23/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MyCollectionViewCell.h"

@protocol imageDownloadHandlerDelegate <NSObject>

- (void)imageDownloadedSuccessfully:(UIImage *)cellImage cellRef:(MyCollectionViewCell *)collectionCell indexRef:(NSInteger)cellIndex;

@end

@interface ImageDownloadHandler : NSObject

@property (nonatomic, retain) UIImage *downloadedImage;
@property (nonatomic, retain) NSMutableDictionary *cachedImage;
@property (nonatomic, weak) id <imageDownloadHandlerDelegate> imageDownloadDelegate;

+ (ImageDownloadHandler *)handlerSharedInstance;
- (void)imageFor:(NSString *)imageUrlStr cellRef:(MyCollectionViewCell *)collectionCell indexRef:(NSInteger)cellIndex;

@end
