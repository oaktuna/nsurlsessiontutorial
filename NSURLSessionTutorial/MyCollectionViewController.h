//
//  MyCollectionViewController.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 24/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyCollectionViewCell.h"
#import "ImageDownloadHandler.h"
#import "NSDictionary+Products.h"
#import "CollectionDetailViewController.h"

@interface MyCollectionViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,imageDownloadHandlerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic, retain) NSString *strTitle;
@property (nonatomic, retain) NSMutableArray *arrItems;

@end
