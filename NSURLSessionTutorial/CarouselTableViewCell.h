//
//  CarouselTableViewCell.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 25/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseSliderController.h"

@interface CarouselTableViewCell : UITableViewCell<BaseSliderControllerDelegate>

@property (nonatomic, strong) BaseSliderController *sliderController;

- (id)initWithArray:(NSString *)reuseIdentifier imageArrayRef:(NSArray *)imagesArray;

@end
