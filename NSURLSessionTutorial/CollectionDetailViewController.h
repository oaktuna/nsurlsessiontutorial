//
//  CollectionDetailViewController.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 25/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CarouselTableViewCell.h"
#import "TextViewCell.h"

@interface CollectionDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblDetailView;
@property (nonatomic) CGRect cellTextSize;

@end
