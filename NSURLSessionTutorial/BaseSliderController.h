//
//  BaseSliderController.h
//  BUYER
//
//  Created by Omer Aktuna on 3/24/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "StyledPageControl.h"

@class BaseSliderController;

@protocol BaseSliderControllerDelegate <NSObject>

- (void)sliderWillBeginDragging:(BaseSliderController *)slider;
- (void)sliderDidEndDecelerating:(BaseSliderController *)slider;
- (void)sliderImageClicked:(UITapGestureRecognizer *)tapGestureRecognizer indexRef:(NSInteger)imageIndex;

@end

@interface BaseSliderController : UIViewController <UIScrollViewDelegate>

@property(nonatomic,strong) StyledPageControl *pageControl;
@property (nonatomic, retain) UIScrollView *scrollView;
@property(nonatomic,retain) NSArray *dataSource;
@property(nonatomic,assign) CGSize containerSize;
@property(nonatomic,assign) CGSize imageSize;
@property(nonatomic,assign) UITableViewCell *cell;

@property (nonatomic, assign) id <BaseSliderControllerDelegate> delegate;
//@property (nonatomic, copy) void (^itemTapped)(UIView *view);

- (id)initWithDataSource:(NSArray*)dataSource size:(CGSize)containerSize imageSize:(CGSize)imageSize;

@end
