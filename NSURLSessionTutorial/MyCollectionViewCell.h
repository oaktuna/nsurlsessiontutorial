//
//  MyCollectionViewCell.h
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 24/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *myImageView;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;


@end
