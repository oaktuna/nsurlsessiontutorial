//
//  ImageDownloadHandler.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 23/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "ImageDownloadHandler.h"

@implementation ImageDownloadHandler

static ImageDownloadHandler *handlerInstance = nil;

+ (ImageDownloadHandler *)handlerSharedInstance {
    @synchronized(self) {
        if (handlerInstance == nil) {
            handlerInstance = [[ImageDownloadHandler alloc] init];
        }
        return handlerInstance;
    }
}

- (void)imageFor:(NSString *)imageUrlStr cellRef:(MyCollectionViewCell *)collectionCell indexRef:(NSInteger)cellIndex {
    
    //NSLog(@"istek edilen image-->%ld",(long)cellIndex);
    
    //__block ListViewController *blockSafeSelf = self;
    
    /*
     NSMutableDictionary ile basit bir cache yapısı kuruldu. Indirilen image'ler "cachedImage" adlı dictionary'iye kayıt edildi.
     Bu dictionary de zaten image varsa download başlatılmadı.
     */
    
    if (self.cachedImage == nil) {
        self.cachedImage = [[NSMutableDictionary alloc] init];
    }
    
    //Varsa indirme, hazırdan ver
    if ([self.cachedImage objectForKey:[NSString stringWithFormat:@"%@%ld",@"item",(long)cellIndex]] != nil) {
       
        if ([self.imageDownloadDelegate respondsToSelector:@selector(imageDownloadedSuccessfully:cellRef:indexRef:)]) {
            [self.imageDownloadDelegate imageDownloadedSuccessfully:[self.cachedImage valueForKey:[NSString stringWithFormat:@"%@%ld",@"item",(long)cellIndex]] cellRef:collectionCell indexRef:cellIndex];
        }
        
    }
    else { //DOWNLOAD IMAGE
        NSURL *url = [NSURL URLWithString:imageUrlStr];
        
            NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
               downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                   
                   UIImage *downloaded_image = [UIImage imageWithData:
                                                [NSData dataWithContentsOfURL:location]];
                   
                   if (downloaded_image != nil) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           self.downloadedImage = downloaded_image;
                           
                           if ([self.cachedImage objectForKey:[NSString stringWithFormat:@"%@%ld",@"item",(long)cellIndex]] == nil) {
                               [self.cachedImage setValue:self.downloadedImage forKey:[NSString stringWithFormat:@"%@%ld",@"item",(long)cellIndex]];
                               
                               if ([self.imageDownloadDelegate respondsToSelector:@selector(imageDownloadedSuccessfully:cellRef:indexRef:)]) {
                                   //NSLog(@"indirilen image-->%ld",(long)cellIndex);
                                   [self.imageDownloadDelegate imageDownloadedSuccessfully:self.downloadedImage cellRef:collectionCell indexRef:cellIndex];
                               }
                           }
                           
                       });
                   }
               }];
            
            [downloadPhotoTask resume];
    }
    
}

@end
