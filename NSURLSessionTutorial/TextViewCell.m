//
//  TextViewCell.m
//  NSURLSessionTutorial
//
//  Created by OMER_AKTUNA on 26/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "TextViewCell.h"

@implementation TextViewCell

- (id)initWithText:(NSString *)cellText reuseRef:(NSString *)reuseIdentifier textSizeRef:(CGRect)cellTextSize {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.autoresizesSubviews = YES;
        [self.contentView setFrame:CGRectMake(0, 0,CGRectGetWidth([[UIScreen mainScreen] bounds]),cellTextSize.size.height)];
        [self setFrame:self.contentView.frame];
        [self.contentView setBackgroundColor:[UIColor yellowColor]];
        
        self.lblText = [[UILabel alloc] init];
        [self.lblText setNumberOfLines:0];
        [self.lblText setFrame:CGRectMake(5.0f, 0.0f, [UIScreen mainScreen].bounds.size.width - 5.0,cellTextSize.size.height)];
        [self.lblText setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
        [self.lblText setTextAlignment:NSTextAlignmentCenter];
        [self.lblText setTextColor:[UIColor blueColor]];
        [self.lblText setText:cellText];
        
        [self addSubview:self.lblText];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
