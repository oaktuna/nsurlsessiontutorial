//
//  NSDictionary+Products.h
//  NSURLSessionTutorial
//
//  Created by Admin on 21/07/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Products)

- (NSArray *)result;
- (NSString *)productName;
- (NSString *)productImage;
- (NSNumber *)productId;

@end
