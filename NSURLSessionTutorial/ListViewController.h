//
//  ViewController.h
//  NSURLSessionTutorial
//
//  Created by Admin on 20/07/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NSDictionary+Products.h"
#import "MyCollectionViewController.h"
#import "ListViewCell.h"

@interface ListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblListView;

@end

